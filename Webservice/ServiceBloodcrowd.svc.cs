﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Hosting;
using System.Windows.Forms;
using System.Xml;

namespace Webservice
{
    // OBSERVAÇÃO: Você pode usar o comando "Renomear" no menu "Refatorar" para alterar o nome da classe "Service1" no arquivo de código, svc e configuração ao mesmo tempo.
    // OBSERVAÇÃO: Para iniciar o cliente de teste do WCF para testar esse serviço, selecione Service1.svc ou Service1.svc.cs no Gerenciador de Soluções e inicie a depuração.
    public class ServiceBloodcrowd : IServiceBloodcrowd
    {
        private static string FILEPATH;

        public ServiceBloodcrowd()
        {
            // Caminho para a base de dados
            FILEPATH = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "App_Data","donors.xml");
        }

        // Faz parsing ao ficheiro .txt vindo da aplicação desktop e constrói um XML que serve de base de dados
        public void BuildXML(string file)
        {
            string readFile = File.ReadAllText(file);

            String[] linhas = readFile.Split('\n');
            String[] header = linhas[0].Split('|');

            header[0] = Regex.Replace(header[0], @"#", "");

            for (int j = 0; j < header.Length; j++)
            {

                header[j] = Regex.Replace(header[j], @"\s+", "");
            }

            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(dec);

            XmlElement root = doc.CreateElement("Blooddonors");
            doc.AppendChild(root);

            for (int i = 1; i < linhas.Length; i++)
            {
                String[] colunas = linhas[i].Split('|');
                XmlElement donor = doc.CreateElement("Donors");
                colunas[0] = Regex.Replace(colunas[0], @"§", "");           

                for (int n = 0; n < colunas.Length; n++)
                {
                    colunas[n] = Regex.Replace(colunas[n], @"\s+", "");
                    

                    XmlElement element = doc.CreateElement(header[n]);
                    element.InnerText = colunas[n];
                    donor.AppendChild(element);
                }

                root.AppendChild(donor);
            }

            doc.Save(FILEPATH);

        }

        // Lista de todos os dadores
        public List<Donor> GetDonors()
        {

            XmlDocument doc = new XmlDocument();
            doc.Load(FILEPATH);

            List<Donor> donors = new List<Donor>();
            XmlNodeList donorNodes = doc.SelectNodes("Blooddonors/Donors");

            foreach (XmlNode donorNode in donorNodes)
            {

                XmlNode numberNode = donorNode.SelectSingleNode("Number");
                XmlNode genderNode = donorNode.SelectSingleNode("Gender");
                XmlNode givenNameNode = donorNode.SelectSingleNode("GivenName");
                XmlNode surnameNode = donorNode.SelectSingleNode("Surname");
                XmlNode streetAddressNode = donorNode.SelectSingleNode("StreetAddress");
                XmlNode cityNode = donorNode.SelectSingleNode("City");
                XmlNode statefullNode = donorNode.SelectSingleNode("StateFull");
                XmlNode zipCodeNode = donorNode.SelectSingleNode("ZipCode");
                XmlNode emailNode = donorNode.SelectSingleNode("EmailAddress");
                XmlNode usernameNode = donorNode.SelectSingleNode("Username");
                XmlNode passwordNode = donorNode.SelectSingleNode("Password");
                XmlNode phoneNumberNode = donorNode.SelectSingleNode("TelephoneNumber");
                XmlNode mothersMaidenNode = donorNode.SelectSingleNode("MothersMaiden");
                XmlNode birthdayNode = donorNode.SelectSingleNode("Birthday");
                XmlNode ageNode = donorNode.SelectSingleNode("Age");
                XmlNode occupationNode = donorNode.SelectSingleNode("Occupation");
                XmlNode companyNode = donorNode.SelectSingleNode("Company");
                XmlNode vehicleNode = donorNode.SelectSingleNode("Vehicle");
                XmlNode bloodTypeNode = donorNode.SelectSingleNode("BloodType");
                XmlNode kilogramsNode = donorNode.SelectSingleNode("Kilograms");
                XmlNode centimetersNode = donorNode.SelectSingleNode("Centimeters");
                XmlNode guidNode = donorNode.SelectSingleNode("GUID");
                XmlNode latitudeNode = donorNode.SelectSingleNode("Latitude");
                XmlNode longitudeNode = donorNode.SelectSingleNode("Longitude");

                Donor donor = new Donor(Convert.ToInt32(numberNode.InnerText),
                                        genderNode.InnerText,
                                        givenNameNode.InnerText,
                                        surnameNode.InnerText,
                                        streetAddressNode.InnerText,
                                        cityNode.InnerText,
                                        statefullNode.InnerText,
                                        zipCodeNode.InnerText,
                                        emailNode.InnerText,
                                        usernameNode.InnerText,
                                        passwordNode.InnerText,
                                        Convert.ToInt64(phoneNumberNode.InnerText),
                                        mothersMaidenNode.InnerText,
                                        birthdayNode.InnerText,
                                        Convert.ToInt32(ageNode.InnerText),
                                        occupationNode.InnerText,
                                        companyNode.InnerText,
                                        vehicleNode.InnerText,
                                        bloodTypeNode.InnerText,
                                        Convert.ToDouble(kilogramsNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToInt32(centimetersNode.InnerText),
                                        guidNode.InnerText,
                                        Convert.ToDouble(latitudeNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToDouble(longitudeNode.InnerText, NumberFormatInfo.InvariantInfo)
                                       );
                donors.Add(donor);
            }

            return donors;
        }

        // Lista todos os dadores por nome
        public List<Donor> GetDonorsByName(string givenName)
       {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILEPATH);

            XmlNodeList donorNodes = doc.SelectNodes("/Blooddonors/Donors[GivenName='" + givenName +"']");
            List<Donor> donors = new List<Donor>();

            foreach (XmlNode donorNode in donorNodes)
            {

                XmlNode numberNode = donorNode.SelectSingleNode("Number");
                XmlNode genderNode = donorNode.SelectSingleNode("Gender");
                XmlNode givenNameNode = donorNode.SelectSingleNode("GivenName");
                XmlNode surnameNode = donorNode.SelectSingleNode("Surname");
                XmlNode streetAddressNode = donorNode.SelectSingleNode("StreetAddress");
                XmlNode cityNode = donorNode.SelectSingleNode("City");
                XmlNode statefullNode = donorNode.SelectSingleNode("StateFull");
                XmlNode zipCodeNode = donorNode.SelectSingleNode("ZipCode");
                XmlNode emailNode = donorNode.SelectSingleNode("EmailAddress");
                XmlNode usernameNode = donorNode.SelectSingleNode("Username");
                XmlNode passwordNode = donorNode.SelectSingleNode("Password");
                XmlNode phoneNumberNode = donorNode.SelectSingleNode("TelephoneNumber");
                XmlNode mothersMaidenNode = donorNode.SelectSingleNode("MothersMaiden");
                XmlNode birthdayNode = donorNode.SelectSingleNode("Birthday");
                XmlNode ageNode = donorNode.SelectSingleNode("Age");
                XmlNode occupationNode = donorNode.SelectSingleNode("Occupation");
                XmlNode companyNode = donorNode.SelectSingleNode("Company");
                XmlNode vehicleNode = donorNode.SelectSingleNode("Vehicle");
                XmlNode bloodTypeNode = donorNode.SelectSingleNode("BloodType");
                XmlNode kilogramsNode = donorNode.SelectSingleNode("Kilograms");
                XmlNode centimetersNode = donorNode.SelectSingleNode("Centimeters");
                XmlNode guidNode = donorNode.SelectSingleNode("GUID");
                XmlNode latitudeNode = donorNode.SelectSingleNode("Latitude");
                XmlNode longitudeNode = donorNode.SelectSingleNode("Longitude");

                Donor donor = new Donor(Convert.ToInt32(numberNode.InnerText),
                                        genderNode.InnerText,
                                        givenNameNode.InnerText,
                                        surnameNode.InnerText,
                                        streetAddressNode.InnerText,
                                        cityNode.InnerText,
                                        statefullNode.InnerText,
                                        zipCodeNode.InnerText,
                                        emailNode.InnerText,
                                        usernameNode.InnerText,
                                        passwordNode.InnerText,
                                        Convert.ToInt64(phoneNumberNode.InnerText),
                                        mothersMaidenNode.InnerText,
                                        birthdayNode.InnerText,
                                        Convert.ToInt32(ageNode.InnerText),
                                        occupationNode.InnerText,
                                        companyNode.InnerText,
                                        vehicleNode.InnerText,
                                        bloodTypeNode.InnerText,
                                        Convert.ToDouble(kilogramsNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToInt32(centimetersNode.InnerText),
                                        guidNode.InnerText,
                                        Convert.ToDouble(latitudeNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToDouble(longitudeNode.InnerText, NumberFormatInfo.InvariantInfo)
                                       );
                donors.Add(donor);
            }
            return donors;
        }

        // Lista todos os dadores por idade
        public List<Donor> GetDonorsByAge(string age)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILEPATH);

            XmlNodeList donorNodes = doc.SelectNodes("/Blooddonors/Donors[Age='" + age + "']");
            List<Donor> donors = new List<Donor>();

            foreach (XmlNode donorNode in donorNodes)
            {

                XmlNode numberNode = donorNode.SelectSingleNode("Number");
                XmlNode genderNode = donorNode.SelectSingleNode("Gender");
                XmlNode givenNameNode = donorNode.SelectSingleNode("GivenName");
                XmlNode surnameNode = donorNode.SelectSingleNode("Surname");
                XmlNode streetAddressNode = donorNode.SelectSingleNode("StreetAddress");
                XmlNode cityNode = donorNode.SelectSingleNode("City");
                XmlNode statefullNode = donorNode.SelectSingleNode("StateFull");
                XmlNode zipCodeNode = donorNode.SelectSingleNode("ZipCode");
                XmlNode emailNode = donorNode.SelectSingleNode("EmailAddress");
                XmlNode usernameNode = donorNode.SelectSingleNode("Username");
                XmlNode passwordNode = donorNode.SelectSingleNode("Password");
                XmlNode phoneNumberNode = donorNode.SelectSingleNode("TelephoneNumber");
                XmlNode mothersMaidenNode = donorNode.SelectSingleNode("MothersMaiden");
                XmlNode birthdayNode = donorNode.SelectSingleNode("Birthday");
                XmlNode ageNode = donorNode.SelectSingleNode("Age");
                XmlNode occupationNode = donorNode.SelectSingleNode("Occupation");
                XmlNode companyNode = donorNode.SelectSingleNode("Company");
                XmlNode vehicleNode = donorNode.SelectSingleNode("Vehicle");
                XmlNode bloodTypeNode = donorNode.SelectSingleNode("BloodType");
                XmlNode kilogramsNode = donorNode.SelectSingleNode("Kilograms");
                XmlNode centimetersNode = donorNode.SelectSingleNode("Centimeters");
                XmlNode guidNode = donorNode.SelectSingleNode("GUID");
                XmlNode latitudeNode = donorNode.SelectSingleNode("Latitude");
                XmlNode longitudeNode = donorNode.SelectSingleNode("Longitude");

                Donor donor = new Donor(Convert.ToInt32(numberNode.InnerText),
                                        genderNode.InnerText,
                                        givenNameNode.InnerText,
                                        surnameNode.InnerText,
                                        streetAddressNode.InnerText,
                                        cityNode.InnerText,
                                        statefullNode.InnerText,
                                        zipCodeNode.InnerText,
                                        emailNode.InnerText,
                                        usernameNode.InnerText,
                                        passwordNode.InnerText,
                                        Convert.ToInt64(phoneNumberNode.InnerText),
                                        mothersMaidenNode.InnerText,
                                        birthdayNode.InnerText,
                                        Convert.ToInt32(ageNode.InnerText),
                                        occupationNode.InnerText,
                                        companyNode.InnerText,
                                        vehicleNode.InnerText,
                                        bloodTypeNode.InnerText,
                                        Convert.ToDouble(kilogramsNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToInt32(centimetersNode.InnerText),
                                        guidNode.InnerText,
                                        Convert.ToDouble(latitudeNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToDouble(longitudeNode.InnerText, NumberFormatInfo.InvariantInfo)
                                       );
                donors.Add(donor);
            }
            return donors;
        }

        // Lista todos os dadores por grupo sanguíneo
        public List<Donor> GetDonorsByBloodType(string bloodType)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILEPATH);

            XmlNodeList donorNodes = doc.SelectNodes("/Blooddonors/Donors[BloodType='" + bloodType + "']");
            List<Donor> donors = new List<Donor>();

            foreach (XmlNode donorNode in donorNodes)
            {

                XmlNode numberNode = donorNode.SelectSingleNode("Number");
                XmlNode genderNode = donorNode.SelectSingleNode("Gender");
                XmlNode givenNameNode = donorNode.SelectSingleNode("GivenName");
                XmlNode surnameNode = donorNode.SelectSingleNode("Surname");
                XmlNode streetAddressNode = donorNode.SelectSingleNode("StreetAddress");
                XmlNode cityNode = donorNode.SelectSingleNode("City");
                XmlNode statefullNode = donorNode.SelectSingleNode("StateFull");
                XmlNode zipCodeNode = donorNode.SelectSingleNode("ZipCode");
                XmlNode emailNode = donorNode.SelectSingleNode("EmailAddress");
                XmlNode usernameNode = donorNode.SelectSingleNode("Username");
                XmlNode passwordNode = donorNode.SelectSingleNode("Password");
                XmlNode phoneNumberNode = donorNode.SelectSingleNode("TelephoneNumber");
                XmlNode mothersMaidenNode = donorNode.SelectSingleNode("MothersMaiden");
                XmlNode birthdayNode = donorNode.SelectSingleNode("Birthday");
                XmlNode ageNode = donorNode.SelectSingleNode("Age");
                XmlNode occupationNode = donorNode.SelectSingleNode("Occupation");
                XmlNode companyNode = donorNode.SelectSingleNode("Company");
                XmlNode vehicleNode = donorNode.SelectSingleNode("Vehicle");
                XmlNode bloodTypeNode = donorNode.SelectSingleNode("BloodType");
                XmlNode kilogramsNode = donorNode.SelectSingleNode("Kilograms");
                XmlNode centimetersNode = donorNode.SelectSingleNode("Centimeters");
                XmlNode guidNode = donorNode.SelectSingleNode("GUID");
                XmlNode latitudeNode = donorNode.SelectSingleNode("Latitude");
                XmlNode longitudeNode = donorNode.SelectSingleNode("Longitude");

                Donor donor = new Donor(Convert.ToInt32(numberNode.InnerText),
                                        genderNode.InnerText,
                                        givenNameNode.InnerText,
                                        surnameNode.InnerText,
                                        streetAddressNode.InnerText,
                                        cityNode.InnerText,
                                        statefullNode.InnerText,
                                        zipCodeNode.InnerText,
                                        emailNode.InnerText,
                                        usernameNode.InnerText,
                                        passwordNode.InnerText,
                                        Convert.ToInt64(phoneNumberNode.InnerText),
                                        mothersMaidenNode.InnerText,
                                        birthdayNode.InnerText,
                                        Convert.ToInt32(ageNode.InnerText),
                                        occupationNode.InnerText,
                                        companyNode.InnerText,
                                        vehicleNode.InnerText,
                                        bloodTypeNode.InnerText,
                                        Convert.ToDouble(kilogramsNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToInt32(centimetersNode.InnerText),
                                        guidNode.InnerText,
                                        Convert.ToDouble(latitudeNode.InnerText, NumberFormatInfo.InvariantInfo),
                                        Convert.ToDouble(longitudeNode.InnerText, NumberFormatInfo.InvariantInfo)
                                       );
                donors.Add(donor);
            }
            return donors;
        }

        // Adiciona um dador
        public void AddDonor(Donor donor)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILEPATH);

            XmlNode root = doc.DocumentElement;
            XmlElement dador = doc.CreateElement("Donors");

            XmlElement numberNode = doc.CreateElement("Number");
            numberNode.InnerText = donor.Number.ToString();
            dador.AppendChild(numberNode);

            XmlElement genderNode = doc.CreateElement("Gender");
            genderNode.InnerText = donor.Gender;
            dador.AppendChild(genderNode);

            XmlElement givenNameNode = doc.CreateElement("GivenName");
            givenNameNode.InnerText = donor.GivenName;
            dador.AppendChild(givenNameNode);

            XmlElement surnameNode = doc.CreateElement("Surname");
            surnameNode.InnerText = donor.Surname;
            dador.AppendChild(surnameNode);

            XmlElement streetAddressNode = doc.CreateElement("StreetAddress");
            streetAddressNode.InnerText = donor.StreetAddress;
            dador.AppendChild(streetAddressNode);

            XmlElement cityNode = doc.CreateElement("City");
            cityNode.InnerText = donor.City;
            dador.AppendChild(cityNode);

            XmlElement statefullNode = doc.CreateElement("StateFull");
            statefullNode.InnerText = donor.StateFull;
            dador.AppendChild(statefullNode);

            XmlElement zipCodeNode = doc.CreateElement("ZipCode");
            zipCodeNode.InnerText = donor.ZipCode;
            dador.AppendChild(zipCodeNode);

            XmlElement emailNode = doc.CreateElement("EmailAddress");
            emailNode.InnerText = donor.Email;
            dador.AppendChild(emailNode);

            XmlElement usernameNode = doc.CreateElement("Username");
            usernameNode.InnerText = donor.Username;
            dador.AppendChild(usernameNode);

            XmlElement passwordNode = doc.CreateElement("Password");
            passwordNode.InnerText = donor.Password;
            dador.AppendChild(passwordNode);

            XmlElement phoneNumberNode = doc.CreateElement("TelephoneNumber");
            phoneNumberNode.InnerText = donor.PhoneNumber.ToString();
            dador.AppendChild(phoneNumberNode);

            XmlElement MothersMaidenNode = doc.CreateElement("MothersMaiden");
            MothersMaidenNode.InnerText = donor.MothersMaiden;
            dador.AppendChild(MothersMaidenNode);

            XmlElement birthdayNode = doc.CreateElement("Birthday");
            birthdayNode.InnerText = donor.Birthday;
            dador.AppendChild(birthdayNode);

            XmlElement ageNode = doc.CreateElement("Age");
            ageNode.InnerText = donor.Age.ToString();
            dador.AppendChild(ageNode);

            XmlElement occupationNode = doc.CreateElement("Occupation");
            occupationNode.InnerText = donor.Occupation;
            dador.AppendChild(occupationNode);

            XmlElement companyNode = doc.CreateElement("Company");
            companyNode.InnerText = donor.Company;
            dador.AppendChild(companyNode);

            XmlElement vehicleNode = doc.CreateElement("Vehicle");
            vehicleNode.InnerText = donor.Vehicle;
            dador.AppendChild(vehicleNode);

            XmlElement bloodTypeNode = doc.CreateElement("BloodType");
            bloodTypeNode.InnerText = donor.BloodType;
            dador.AppendChild(bloodTypeNode);

            XmlElement kilogramsNode = doc.CreateElement("Kilograms");
            kilogramsNode.InnerText = donor.Kilograms.ToString();
            dador.AppendChild(kilogramsNode);

            XmlElement centimetersNode = doc.CreateElement("Centimeters");
            centimetersNode.InnerText = donor.Centimeters.ToString();
            dador.AppendChild(centimetersNode);

            XmlElement guidNode = doc.CreateElement("GUID");
            guidNode.InnerText = donor.Guid;
            dador.AppendChild(guidNode);

            XmlElement latitudeNode = doc.CreateElement("Latitude");
            latitudeNode.InnerText = donor.Latitude.ToString();
            dador.AppendChild(latitudeNode);

            XmlElement longitudeNode = doc.CreateElement("Longitude");
            longitudeNode.InnerText = donor.Longitude.ToString();
            dador.AppendChild(longitudeNode);

            root.AppendChild(dador);
            doc.Save(FILEPATH);

        }

        // Devolve um único dador por número
        public Donor GetDonor(string id)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILEPATH);

            XmlNode numberNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Number");
            XmlNode genderNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Gender");
            XmlNode givenNameNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/GivenName");
            XmlNode surnameNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Surname");
            XmlNode streetAddressNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/StreetAddress");
            XmlNode cityNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/City");
            XmlNode statefullNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/StateFull");
            XmlNode zipCodeNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/ZipCode");
            XmlNode emailNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/EmailAddress");
            XmlNode usernameNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Username");
            XmlNode passwordNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Password");
            XmlNode phoneNumberNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/TelephoneNumber");
            XmlNode mothersMaidenNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/MothersMaiden");
            XmlNode birthdayNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Birthday");
            XmlNode ageNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Age");
            XmlNode occupationNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Occupation");
            XmlNode companyNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Company");
            XmlNode vehicleNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Vehicle");
            XmlNode bloodTypeNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/BloodType");
            XmlNode kilogramsNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Kilograms");
            XmlNode centimetersNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Centimeters");
            XmlNode guidNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/GUID");
            XmlNode latitudeNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Latitude");
            XmlNode longitudeNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + id + "']/Longitude");


            Donor donor = new Donor(Convert.ToInt32(numberNode.InnerText),
                                         genderNode.InnerText,
                                         givenNameNode.InnerText,
                                         surnameNode.InnerText,
                                         streetAddressNode.InnerText,
                                         cityNode.InnerText,
                                         statefullNode.InnerText,
                                         zipCodeNode.InnerText,
                                         emailNode.InnerText,
                                         usernameNode.InnerText,
                                         passwordNode.InnerText,
                                         Convert.ToInt64(phoneNumberNode.InnerText),
                                         mothersMaidenNode.InnerText,
                                         birthdayNode.InnerText,
                                         Convert.ToInt32(ageNode.InnerText),
                                         occupationNode.InnerText,
                                         companyNode.InnerText,
                                         vehicleNode.InnerText,
                                         bloodTypeNode.InnerText,
                                         Convert.ToDouble(kilogramsNode.InnerText, NumberFormatInfo.InvariantInfo),
                                         Convert.ToInt32(centimetersNode.InnerText),
                                         guidNode.InnerText,
                                         Convert.ToDouble(latitudeNode.InnerText, NumberFormatInfo.InvariantInfo),
                                         Convert.ToDouble(longitudeNode.InnerText, NumberFormatInfo.InvariantInfo)
                                        );
            return donor;
        }

        // Devolve o número do último dador
        public string GetLastNumber()
        {
            XmlDocument doc = new XmlDocument();  
            doc.Load(FILEPATH);

            XmlNode bloodDonorsNode = doc.SelectSingleNode("/Blooddonors");
            XmlNode donorNode = doc.SelectSingleNode("/Blooddonors/Donors[last()]/Number");

            string number = donorNode.InnerText;
            return number;
        }
    
        // Remove um único dador por número
        public void DeleteDonor(string number)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(FILEPATH);

            XmlNode bloodDonorsNode = doc.SelectSingleNode("/Blooddonors");
            XmlNode donorNode = doc.SelectSingleNode("/Blooddonors/Donors[Number='" + number + "']");

            if (donorNode != null)
            {
                bloodDonorsNode.RemoveChild(donorNode);
                doc.Save(FILEPATH);
            }
        }

        // Gera palavra-passe aleatória
        public string GeneratePassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var password = new String(stringChars);
            return password;
        }
    }
}
