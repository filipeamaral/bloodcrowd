﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Webservice
{
    [ServiceContract]
    public interface IServiceBloodcrowd
    {
        // BUILD XML
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/xml")]
        [Description("Builds XML file.")]
        void BuildXML(string path);

        // GET DONORS
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/donors")]
        [Description("Gets all donors.")]
        List<Donor> GetDonors();

        [OperationContract(Name = "GetDonorByNumber")]
        [WebInvoke(Method = "GET", UriTemplate = "/donor/{number}")]
        [Description("Gets a donor by Number.")]
        Donor GetDonor(string id);

        [OperationContract(Name = "GetDonorsByName")]
        [WebInvoke(Method = "GET", UriTemplate = "/donors/{givenName}")]
        [Description("Gets all the Donors by Given Name.")]
        List<Donor> GetDonorsByName(string givenName);

        [OperationContract(Name = "GetDonorsByAge")]
        [WebInvoke(Method = "GET", UriTemplate = "/donors/{age}")]
        [Description("Gets all the donors by Age.")]
        List<Donor> GetDonorsByAge(string age);

        [OperationContract(Name = "GetDonorsByBloodType")]
        [WebInvoke(Method = "GET", UriTemplate = "/donors/{bloodType}")]
        [Description("Gets all the donors by Blood Type.")]
        List<Donor> GetDonorsByBloodType(string bloodType);

        // GET LAST NUMBER
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/donors/lastNumber")]
        [Description("Gets last donors number.")]
        string GetLastNumber();

        // GENERATE PASSWORD
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/donors/genPassword")]
        [Description("Generates a random password.")]
        string GeneratePassword();

        // ADD DONOR
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/donors")]
        [Description("Add Donor.")]
        void AddDonor(Donor donor);

        // DELETE DONOR
        [OperationContract(Name = "DeleteDonorByNumber")]
        [WebInvoke(Method = "DELETE", UriTemplate = "/donor/{number}")]
        [Description("Delete Donor by Number.")]
        void DeleteDonor(string number);
    }

    [DataContract]
    public class Donor
    {
        public Donor()
        {
        }

        public Donor(int number, string gender, string givenName, string surname, string streetAddress,
                    string city, string stateFull, string zipCode, string email, string username,
                    string password, long phoneNumber, string mothersMaiden, string birthday,
                    int age, string occupation, string company, string vehicle, string bloodType,
                    double kilograms, int centimeters, string guid, double latitude, double longitude)
        {

            this.Number = number;
            this.Gender = gender;
            this.GivenName = givenName;
            this.Surname = surname;
            this.StreetAddress = streetAddress;
            this.City = city;
            this.StateFull = stateFull;
            this.ZipCode = zipCode;
            this.Email = email;
            this.Username = username;
            this.Password = password;
            this.PhoneNumber = phoneNumber;
            this.MothersMaiden = mothersMaiden;
            this.Birthday = birthday;
            this.Age = age;
            this.Occupation = occupation;
            this.Company = company;
            this.Vehicle = vehicle;
            this.BloodType = bloodType;
            this.Kilograms = kilograms;
            this.Centimeters = centimeters;
            this.Guid = guid;
            this.Latitude = latitude;
            this.Longitude = longitude;
            
        }
        [DataMember]
        public int Number { get; set; }
        [DataMember]
        public string Gender { get; set; }
        [DataMember]
        public string GivenName { get; set; }
        [DataMember]
        public string Surname { get; set; }
        [DataMember]
        public string StreetAddress { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string StateFull { get; set; }
        [DataMember]
        public string ZipCode { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public long PhoneNumber { get; set; }
        [DataMember]
        public string MothersMaiden { get; set; }
        [DataMember]
        public string Birthday { get; set; }
        [DataMember]
        public int Age { get; set; }
        [DataMember]
        public string Occupation { get; set; }
        [DataMember]
        public string Company { get; set; }
        [DataMember]
        public string Vehicle { get; set; }
        [DataMember]
        public string BloodType { get; set; }
        [DataMember]
        public double Kilograms { get; set; }
        [DataMember]
        public int Centimeters { get; set; }
        [DataMember]
        public string Guid { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public double Longitude { get; set; }     

        public override string ToString()
        {
            string res = string.Empty; res += "Given name: " + GivenName + Environment.NewLine; res += "Surname: " + Surname
                + Environment.NewLine; res += "Street Address: " + StreetAddress + Environment.NewLine; res += "City: " + City
                + Environment.NewLine; res += "StateFull: " + StateFull + Environment.NewLine; res += "Zip Code: " + ZipCode 
                + Environment.NewLine; res += "Email: " + Email + Environment.NewLine; res += "Username: " + Username 
                + Environment.NewLine; res += "Password: " + Password + Environment.NewLine; res += "Telephone Number: " + PhoneNumber 
                + Environment.NewLine; res += "Mothers Maiden: " + MothersMaiden + Environment.NewLine; res += "Birthday: " + Birthday 
                + Environment.NewLine; res += "Age: " + Age + Environment.NewLine; res += "Occupation: " + Occupation 
                + Environment.NewLine; res += "Company: " + Company + Environment.NewLine; res += "Vehicle: " + Vehicle 
                + Environment.NewLine; res += "Blood Type: " + BloodType + Environment.NewLine; res += "Kilograms: " + Kilograms 
                + Environment.NewLine; res += "Centimeters: " + Centimeters + Environment.NewLine; res += "GUID: " + Guid 
                + Environment.NewLine; res += "Latitude: " + Latitude + Environment.NewLine; res += "Longitude: " + Longitude 
                + Environment.NewLine;

            return res;
        }
    }
}
