﻿namespace ClientDesktop
{
    partial class AddDonor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.genderLabel = new System.Windows.Forms.Label();
            this.labelLongitude = new System.Windows.Forms.Label();
            this.longitudeTextBox = new System.Windows.Forms.RichTextBox();
            this.labelLatitude = new System.Windows.Forms.Label();
            this.latitudeTextBox = new System.Windows.Forms.RichTextBox();
            this.labelBloodType = new System.Windows.Forms.Label();
            this.labelVehicle = new System.Windows.Forms.Label();
            this.vehicleTextBox = new System.Windows.Forms.RichTextBox();
            this.labelCompany = new System.Windows.Forms.Label();
            this.companyTextBox = new System.Windows.Forms.RichTextBox();
            this.labelGUID = new System.Windows.Forms.Label();
            this.guidTextBox = new System.Windows.Forms.RichTextBox();
            this.labelCentimeters = new System.Windows.Forms.Label();
            this.centimetersTextBox = new System.Windows.Forms.RichTextBox();
            this.labelKilograms = new System.Windows.Forms.Label();
            this.kilogramsTextBox = new System.Windows.Forms.RichTextBox();
            this.labelOccupation = new System.Windows.Forms.Label();
            this.occupationTextBox = new System.Windows.Forms.RichTextBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.labelMothersMaiden = new System.Windows.Forms.Label();
            this.mothersMaidenTextBox = new System.Windows.Forms.RichTextBox();
            this.labelTelephone = new System.Windows.Forms.Label();
            this.telephoneTextBox = new System.Windows.Forms.RichTextBox();
            this.labelUsername = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.RichTextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.RichTextBox();
            this.labelZipCode = new System.Windows.Forms.Label();
            this.zipCodeTextBox = new System.Windows.Forms.RichTextBox();
            this.labelStatefull = new System.Windows.Forms.Label();
            this.statefullTextBox = new System.Windows.Forms.RichTextBox();
            this.labelCidade = new System.Windows.Forms.Label();
            this.cityTextBox = new System.Windows.Forms.RichTextBox();
            this.labelMorada = new System.Windows.Forms.Label();
            this.streetAddressTextBox = new System.Windows.Forms.RichTextBox();
            this.labelFullName = new System.Windows.Forms.Label();
            this.givenNameTextBox = new System.Windows.Forms.RichTextBox();
            this.buttonAddDonor = new System.Windows.Forms.Button();
            this.labelSurname = new System.Windows.Forms.Label();
            this.surnameTextBox = new System.Windows.Forms.RichTextBox();
            this.genderComboBox = new System.Windows.Forms.ComboBox();
            this.bloodTypeComboBox = new System.Windows.Forms.ComboBox();
            this.birthdayDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // genderLabel
            // 
            this.genderLabel.AutoSize = true;
            this.genderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genderLabel.Location = new System.Drawing.Point(76, 100);
            this.genderLabel.Name = "genderLabel";
            this.genderLabel.Size = new System.Drawing.Size(39, 16);
            this.genderLabel.TabIndex = 85;
            this.genderLabel.Text = "Sexo";
            // 
            // labelLongitude
            // 
            this.labelLongitude.AutoSize = true;
            this.labelLongitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLongitude.Location = new System.Drawing.Point(472, 298);
            this.labelLongitude.Name = "labelLongitude";
            this.labelLongitude.Size = new System.Drawing.Size(67, 16);
            this.labelLongitude.TabIndex = 83;
            this.labelLongitude.Text = "Longitude";
            // 
            // longitudeTextBox
            // 
            this.longitudeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.longitudeTextBox.Location = new System.Drawing.Point(545, 295);
            this.longitudeTextBox.Name = "longitudeTextBox";
            this.longitudeTextBox.Size = new System.Drawing.Size(254, 26);
            this.longitudeTextBox.TabIndex = 82;
            this.longitudeTextBox.Text = "";
            // 
            // labelLatitude
            // 
            this.labelLatitude.AutoSize = true;
            this.labelLatitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLatitude.Location = new System.Drawing.Point(484, 331);
            this.labelLatitude.Name = "labelLatitude";
            this.labelLatitude.Size = new System.Drawing.Size(55, 16);
            this.labelLatitude.TabIndex = 81;
            this.labelLatitude.Text = "Latitude";
            // 
            // latitudeTextBox
            // 
            this.latitudeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.latitudeTextBox.Location = new System.Drawing.Point(545, 328);
            this.latitudeTextBox.Name = "latitudeTextBox";
            this.latitudeTextBox.Size = new System.Drawing.Size(254, 27);
            this.latitudeTextBox.TabIndex = 80;
            this.latitudeTextBox.Text = "";
            // 
            // labelBloodType
            // 
            this.labelBloodType.AutoSize = true;
            this.labelBloodType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBloodType.Location = new System.Drawing.Point(426, 265);
            this.labelBloodType.Name = "labelBloodType";
            this.labelBloodType.Size = new System.Drawing.Size(113, 16);
            this.labelBloodType.TabIndex = 79;
            this.labelBloodType.Text = "Grupo Sanguíneo";
            // 
            // labelVehicle
            // 
            this.labelVehicle.AutoSize = true;
            this.labelVehicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVehicle.Location = new System.Drawing.Point(486, 231);
            this.labelVehicle.Name = "labelVehicle";
            this.labelVehicle.Size = new System.Drawing.Size(53, 16);
            this.labelVehicle.TabIndex = 77;
            this.labelVehicle.Text = "Veículo";
            // 
            // vehicleTextBox
            // 
            this.vehicleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vehicleTextBox.Location = new System.Drawing.Point(545, 228);
            this.vehicleTextBox.Name = "vehicleTextBox";
            this.vehicleTextBox.Size = new System.Drawing.Size(254, 27);
            this.vehicleTextBox.TabIndex = 76;
            this.vehicleTextBox.Text = "";
            // 
            // labelCompany
            // 
            this.labelCompany.AutoSize = true;
            this.labelCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompany.Location = new System.Drawing.Point(476, 199);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(63, 16);
            this.labelCompany.TabIndex = 75;
            this.labelCompany.Text = "Empresa";
            // 
            // companyTextBox
            // 
            this.companyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyTextBox.Location = new System.Drawing.Point(545, 196);
            this.companyTextBox.Name = "companyTextBox";
            this.companyTextBox.Size = new System.Drawing.Size(254, 26);
            this.companyTextBox.TabIndex = 74;
            this.companyTextBox.Text = "";
            // 
            // labelGUID
            // 
            this.labelGUID.AutoSize = true;
            this.labelGUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGUID.Location = new System.Drawing.Point(74, 362);
            this.labelGUID.Name = "labelGUID";
            this.labelGUID.Size = new System.Drawing.Size(41, 16);
            this.labelGUID.TabIndex = 73;
            this.labelGUID.Text = "GUID";
            // 
            // guidTextBox
            // 
            this.guidTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guidTextBox.Location = new System.Drawing.Point(121, 359);
            this.guidTextBox.Name = "guidTextBox";
            this.guidTextBox.Size = new System.Drawing.Size(254, 26);
            this.guidTextBox.TabIndex = 72;
            this.guidTextBox.Text = "";
            // 
            // labelCentimeters
            // 
            this.labelCentimeters.AutoSize = true;
            this.labelCentimeters.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCentimeters.Location = new System.Drawing.Point(44, 331);
            this.labelCentimeters.Name = "labelCentimeters";
            this.labelCentimeters.Size = new System.Drawing.Size(71, 16);
            this.labelCentimeters.TabIndex = 71;
            this.labelCentimeters.Text = "Altura (cm)";
            // 
            // centimetersTextBox
            // 
            this.centimetersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centimetersTextBox.Location = new System.Drawing.Point(121, 326);
            this.centimetersTextBox.Name = "centimetersTextBox";
            this.centimetersTextBox.Size = new System.Drawing.Size(254, 27);
            this.centimetersTextBox.TabIndex = 70;
            this.centimetersTextBox.Text = "";
            // 
            // labelKilograms
            // 
            this.labelKilograms.AutoSize = true;
            this.labelKilograms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKilograms.Location = new System.Drawing.Point(49, 298);
            this.labelKilograms.Name = "labelKilograms";
            this.labelKilograms.Size = new System.Drawing.Size(66, 16);
            this.labelKilograms.TabIndex = 69;
            this.labelKilograms.Text = "Peso (kg)";
            // 
            // kilogramsTextBox
            // 
            this.kilogramsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kilogramsTextBox.Location = new System.Drawing.Point(121, 293);
            this.kilogramsTextBox.Name = "kilogramsTextBox";
            this.kilogramsTextBox.Size = new System.Drawing.Size(254, 27);
            this.kilogramsTextBox.TabIndex = 68;
            this.kilogramsTextBox.Text = "";
            // 
            // labelOccupation
            // 
            this.labelOccupation.AutoSize = true;
            this.labelOccupation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOccupation.Location = new System.Drawing.Point(468, 166);
            this.labelOccupation.Name = "labelOccupation";
            this.labelOccupation.Size = new System.Drawing.Size(71, 16);
            this.labelOccupation.TabIndex = 67;
            this.labelOccupation.Text = "Ocupação";
            // 
            // occupationTextBox
            // 
            this.occupationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.occupationTextBox.Location = new System.Drawing.Point(545, 163);
            this.occupationTextBox.Name = "occupationTextBox";
            this.occupationTextBox.Size = new System.Drawing.Size(254, 27);
            this.occupationTextBox.TabIndex = 66;
            this.occupationTextBox.Text = "";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBirthday.Location = new System.Drawing.Point(408, 133);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(131, 16);
            this.labelBirthday.TabIndex = 63;
            this.labelBirthday.Text = "Data de Nascimento";
            // 
            // labelMothersMaiden
            // 
            this.labelMothersMaiden.AutoSize = true;
            this.labelMothersMaiden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMothersMaiden.Location = new System.Drawing.Point(445, 100);
            this.labelMothersMaiden.Name = "labelMothersMaiden";
            this.labelMothersMaiden.Size = new System.Drawing.Size(94, 16);
            this.labelMothersMaiden.TabIndex = 61;
            this.labelMothersMaiden.Text = "Nome da Mãe";
            // 
            // mothersMaidenTextBox
            // 
            this.mothersMaidenTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mothersMaidenTextBox.Location = new System.Drawing.Point(545, 97);
            this.mothersMaidenTextBox.Name = "mothersMaidenTextBox";
            this.mothersMaidenTextBox.Size = new System.Drawing.Size(254, 26);
            this.mothersMaidenTextBox.TabIndex = 60;
            this.mothersMaidenTextBox.Text = "";
            // 
            // labelTelephone
            // 
            this.labelTelephone.AutoSize = true;
            this.labelTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTelephone.Location = new System.Drawing.Point(477, 67);
            this.labelTelephone.Name = "labelTelephone";
            this.labelTelephone.Size = new System.Drawing.Size(62, 16);
            this.labelTelephone.TabIndex = 59;
            this.labelTelephone.Text = "Telefone";
            // 
            // telephoneTextBox
            // 
            this.telephoneTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telephoneTextBox.Location = new System.Drawing.Point(545, 64);
            this.telephoneTextBox.Name = "telephoneTextBox";
            this.telephoneTextBox.Size = new System.Drawing.Size(254, 26);
            this.telephoneTextBox.TabIndex = 58;
            this.telephoneTextBox.Text = "";
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUsername.Location = new System.Drawing.Point(475, 35);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(64, 16);
            this.labelUsername.TabIndex = 57;
            this.labelUsername.Text = "Utilizador";
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameTextBox.Location = new System.Drawing.Point(545, 32);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(254, 26);
            this.usernameTextBox.TabIndex = 56;
            this.usernameTextBox.Text = "";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.Location = new System.Drawing.Point(73, 265);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(42, 16);
            this.labelEmail.TabIndex = 55;
            this.labelEmail.Text = "Email";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailTextBox.Location = new System.Drawing.Point(121, 260);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(254, 27);
            this.emailTextBox.TabIndex = 54;
            this.emailTextBox.Text = "";
            // 
            // labelZipCode
            // 
            this.labelZipCode.AutoSize = true;
            this.labelZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZipCode.Location = new System.Drawing.Point(22, 231);
            this.labelZipCode.Name = "labelZipCode";
            this.labelZipCode.Size = new System.Drawing.Size(93, 16);
            this.labelZipCode.TabIndex = 53;
            this.labelZipCode.Text = "Código Postal";
            // 
            // zipCodeTextBox
            // 
            this.zipCodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zipCodeTextBox.Location = new System.Drawing.Point(121, 228);
            this.zipCodeTextBox.Name = "zipCodeTextBox";
            this.zipCodeTextBox.Size = new System.Drawing.Size(254, 26);
            this.zipCodeTextBox.TabIndex = 52;
            this.zipCodeTextBox.Text = "";
            // 
            // labelStatefull
            // 
            this.labelStatefull.AutoSize = true;
            this.labelStatefull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatefull.Location = new System.Drawing.Point(66, 199);
            this.labelStatefull.Name = "labelStatefull";
            this.labelStatefull.Size = new System.Drawing.Size(49, 16);
            this.labelStatefull.TabIndex = 51;
            this.labelStatefull.Text = "Distrito";
            // 
            // statefullTextBox
            // 
            this.statefullTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statefullTextBox.Location = new System.Drawing.Point(121, 195);
            this.statefullTextBox.Name = "statefullTextBox";
            this.statefullTextBox.Size = new System.Drawing.Size(254, 27);
            this.statefullTextBox.TabIndex = 50;
            this.statefullTextBox.Text = "";
            // 
            // labelCidade
            // 
            this.labelCidade.AutoSize = true;
            this.labelCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCidade.Location = new System.Drawing.Point(63, 165);
            this.labelCidade.Name = "labelCidade";
            this.labelCidade.Size = new System.Drawing.Size(52, 16);
            this.labelCidade.TabIndex = 49;
            this.labelCidade.Text = "Cidade";
            // 
            // cityTextBox
            // 
            this.cityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityTextBox.Location = new System.Drawing.Point(121, 162);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.Size = new System.Drawing.Size(254, 27);
            this.cityTextBox.TabIndex = 48;
            this.cityTextBox.Text = "";
            // 
            // labelMorada
            // 
            this.labelMorada.AutoSize = true;
            this.labelMorada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMorada.Location = new System.Drawing.Point(60, 132);
            this.labelMorada.Name = "labelMorada";
            this.labelMorada.Size = new System.Drawing.Size(55, 16);
            this.labelMorada.TabIndex = 47;
            this.labelMorada.Text = "Morada";
            // 
            // streetAddressTextBox
            // 
            this.streetAddressTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.streetAddressTextBox.Location = new System.Drawing.Point(121, 129);
            this.streetAddressTextBox.Name = "streetAddressTextBox";
            this.streetAddressTextBox.Size = new System.Drawing.Size(254, 27);
            this.streetAddressTextBox.TabIndex = 46;
            this.streetAddressTextBox.Text = "";
            // 
            // labelFullName
            // 
            this.labelFullName.AutoSize = true;
            this.labelFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFullName.Location = new System.Drawing.Point(70, 35);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(45, 16);
            this.labelFullName.TabIndex = 45;
            this.labelFullName.Text = "Nome";
            // 
            // givenNameTextBox
            // 
            this.givenNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.givenNameTextBox.Location = new System.Drawing.Point(121, 32);
            this.givenNameTextBox.Name = "givenNameTextBox";
            this.givenNameTextBox.Size = new System.Drawing.Size(254, 26);
            this.givenNameTextBox.TabIndex = 44;
            this.givenNameTextBox.Text = "";
            // 
            // buttonAddDonor
            // 
            this.buttonAddDonor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddDonor.Location = new System.Drawing.Point(702, 392);
            this.buttonAddDonor.Name = "buttonAddDonor";
            this.buttonAddDonor.Size = new System.Drawing.Size(97, 27);
            this.buttonAddDonor.TabIndex = 86;
            this.buttonAddDonor.Text = "Adicionar";
            this.buttonAddDonor.UseVisualStyleBackColor = true;
            this.buttonAddDonor.Click += new System.EventHandler(this.buttonAddDonor_Click);
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSurname.Location = new System.Drawing.Point(60, 67);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(55, 16);
            this.labelSurname.TabIndex = 88;
            this.labelSurname.Text = "Apelido";
            // 
            // surnameTextBox
            // 
            this.surnameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.surnameTextBox.Location = new System.Drawing.Point(121, 64);
            this.surnameTextBox.Name = "surnameTextBox";
            this.surnameTextBox.Size = new System.Drawing.Size(254, 26);
            this.surnameTextBox.TabIndex = 87;
            this.surnameTextBox.Text = "";
            // 
            // genderComboBox
            // 
            this.genderComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genderComboBox.FormattingEnabled = true;
            this.genderComboBox.Location = new System.Drawing.Point(121, 96);
            this.genderComboBox.Name = "genderComboBox";
            this.genderComboBox.Size = new System.Drawing.Size(254, 23);
            this.genderComboBox.TabIndex = 89;
            this.genderComboBox.SelectedIndexChanged += new System.EventHandler(this.genderComboBox_SelectedIndexChanged);
            // 
            // bloodTypeComboBox
            // 
            this.bloodTypeComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bloodTypeComboBox.FormattingEnabled = true;
            this.bloodTypeComboBox.ItemHeight = 15;
            this.bloodTypeComboBox.Location = new System.Drawing.Point(545, 262);
            this.bloodTypeComboBox.Name = "bloodTypeComboBox";
            this.bloodTypeComboBox.Size = new System.Drawing.Size(254, 23);
            this.bloodTypeComboBox.TabIndex = 90;
            this.bloodTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.bloodTypeComboBox_SelectedIndexChanged);
            // 
            // birthdayDateTimePicker
            // 
            this.birthdayDateTimePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthdayDateTimePicker.Location = new System.Drawing.Point(545, 132);
            this.birthdayDateTimePicker.Name = "birthdayDateTimePicker";
            this.birthdayDateTimePicker.Size = new System.Drawing.Size(254, 22);
            this.birthdayDateTimePicker.TabIndex = 91;
            // 
            // AddDonor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(861, 440);
            this.Controls.Add(this.birthdayDateTimePicker);
            this.Controls.Add(this.bloodTypeComboBox);
            this.Controls.Add(this.genderComboBox);
            this.Controls.Add(this.labelSurname);
            this.Controls.Add(this.surnameTextBox);
            this.Controls.Add(this.buttonAddDonor);
            this.Controls.Add(this.genderLabel);
            this.Controls.Add(this.labelLongitude);
            this.Controls.Add(this.longitudeTextBox);
            this.Controls.Add(this.labelLatitude);
            this.Controls.Add(this.latitudeTextBox);
            this.Controls.Add(this.labelBloodType);
            this.Controls.Add(this.labelVehicle);
            this.Controls.Add(this.vehicleTextBox);
            this.Controls.Add(this.labelCompany);
            this.Controls.Add(this.companyTextBox);
            this.Controls.Add(this.labelGUID);
            this.Controls.Add(this.guidTextBox);
            this.Controls.Add(this.labelCentimeters);
            this.Controls.Add(this.centimetersTextBox);
            this.Controls.Add(this.labelKilograms);
            this.Controls.Add(this.kilogramsTextBox);
            this.Controls.Add(this.labelOccupation);
            this.Controls.Add(this.occupationTextBox);
            this.Controls.Add(this.labelBirthday);
            this.Controls.Add(this.labelMothersMaiden);
            this.Controls.Add(this.mothersMaidenTextBox);
            this.Controls.Add(this.labelTelephone);
            this.Controls.Add(this.telephoneTextBox);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.labelZipCode);
            this.Controls.Add(this.zipCodeTextBox);
            this.Controls.Add(this.labelStatefull);
            this.Controls.Add(this.statefullTextBox);
            this.Controls.Add(this.labelCidade);
            this.Controls.Add(this.cityTextBox);
            this.Controls.Add(this.labelMorada);
            this.Controls.Add(this.streetAddressTextBox);
            this.Controls.Add(this.labelFullName);
            this.Controls.Add(this.givenNameTextBox);
            this.Name = "AddDonor";
            this.Text = "Adicionar dador";
            this.Load += new System.EventHandler(this.AddDonor_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label genderLabel;
        private System.Windows.Forms.Label labelLongitude;
        private System.Windows.Forms.RichTextBox longitudeTextBox;
        private System.Windows.Forms.Label labelLatitude;
        private System.Windows.Forms.RichTextBox latitudeTextBox;
        private System.Windows.Forms.Label labelBloodType;
        private System.Windows.Forms.Label labelVehicle;
        private System.Windows.Forms.RichTextBox vehicleTextBox;
        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.RichTextBox companyTextBox;
        private System.Windows.Forms.Label labelGUID;
        private System.Windows.Forms.RichTextBox guidTextBox;
        private System.Windows.Forms.Label labelCentimeters;
        private System.Windows.Forms.RichTextBox centimetersTextBox;
        private System.Windows.Forms.Label labelKilograms;
        private System.Windows.Forms.RichTextBox kilogramsTextBox;
        private System.Windows.Forms.Label labelOccupation;
        private System.Windows.Forms.RichTextBox occupationTextBox;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Label labelMothersMaiden;
        private System.Windows.Forms.RichTextBox mothersMaidenTextBox;
        private System.Windows.Forms.Label labelTelephone;
        private System.Windows.Forms.RichTextBox telephoneTextBox;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.RichTextBox usernameTextBox;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.RichTextBox emailTextBox;
        private System.Windows.Forms.Label labelZipCode;
        private System.Windows.Forms.RichTextBox zipCodeTextBox;
        private System.Windows.Forms.Label labelStatefull;
        private System.Windows.Forms.RichTextBox statefullTextBox;
        private System.Windows.Forms.Label labelCidade;
        private System.Windows.Forms.RichTextBox cityTextBox;
        private System.Windows.Forms.Label labelMorada;
        private System.Windows.Forms.RichTextBox streetAddressTextBox;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.RichTextBox givenNameTextBox;
        private System.Windows.Forms.Button buttonAddDonor;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.RichTextBox surnameTextBox;
        private System.Windows.Forms.ComboBox genderComboBox;
        private System.Windows.Forms.ComboBox bloodTypeComboBox;
        private System.Windows.Forms.DateTimePicker birthdayDateTimePicker;
    }
}