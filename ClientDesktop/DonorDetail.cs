﻿using ClientDesktop.ServiceBloodcrowd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientDesktop
{
    public partial class DonorDetail : Form
    {
        ServiceBloodcrowdClient client = new ServiceBloodcrowdClient();

        public DonorDetail()
        {
            InitializeComponent();
        }

        public void ShowDonorDetail(string id)
        {
            
            Donor donor = client.GetDonorByNumber(id);

            int number = donor.Number;
            String gender = donor.Gender;
            String givenName = donor.GivenName;
            String surname = donor.Surname;
            String streetAddress = donor.StreetAddress;
            String city = donor.City;
            String stateFull = donor.StateFull;
            String zipCode = donor.ZipCode;
            String email = donor.Email;
            String username = donor.Username;
            String password = donor.Password;
            long phoneNumber = donor.PhoneNumber;
            String mothersMaiden = donor.MothersMaiden;
            String birthday = donor.Birthday;
            int age = donor.Age;
            String occupation = donor.Occupation;
            String company = donor.Company;
            String vehicle = donor.Vehicle;
            String bloodType = donor.BloodType;
            double kilograms = donor.Kilograms;
            int centimeters = donor.Centimeters;
            String guid = donor.Guid;
            double latitude = donor.Latitude;
            double longitude = donor.Longitude;

            fullNameTextBox.Text = givenName + " " + surname;
            genderTextBox.Text = gender;
            streetAddressTextBox.Text = streetAddress;
            cityTextBox.Text = city;
            statefullTextBox.Text = stateFull;
            zipCodeTextBox.Text = zipCode;
            emailTextBox.Text = email;
            usernameTextBox.Text = username;
            telephoneTextBox.Text = phoneNumber.ToString();
            mothersMaidenTextBox.Text = mothersMaiden;
            birthdayTextBox.Text = birthday;
            ageTextBox.Text = age.ToString();
            occupationTextBox.Text = occupation;
            companyTextBox.Text = company;
            vehicleTextBox.Text = vehicle;
            bloodtypeTextBox.Text = bloodType;
            kilogramsTextBox.Text = kilograms.ToString();
            centimetersTextBox.Text = centimeters.ToString();
            guidTextBox.Text = guid;
            latitudeTextBox.Text = latitude.ToString();
            longitudeTextBox.Text = longitude.ToString();

        }

        private void detailDonorBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void DonorDetail_Load(object sender, EventArgs e)
        {

        }

        private void richTextBox20_TextChanged(object sender, EventArgs e)
        {

        }

        private void cityTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void labelUsername_Click(object sender, EventArgs e)
        {

        }

        private void guidTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
