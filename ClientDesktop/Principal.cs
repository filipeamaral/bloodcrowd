﻿using ClientDesktop.ServiceBloodcrowd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ClientDesktop
{
    public partial class Principal : Form
    {
        DonorDetail d = new DonorDetail();
        AddDonor ad = new AddDonor();
        Controller c = new Controller();
        ServiceBloodcrowdClient client = new ServiceBloodcrowdClient(); 

        public Principal()
        {
            InitializeComponent();
        }

        private void importDonors_Click(object sender, EventArgs e)
        {

            // Abre ficheiro .txt e envia para o webservice
            Stream myStream = null;
            OpenFileDialog open = new OpenFileDialog();
            open.Title = "Abrir ficheiro de texto";
            open.Filter = "TXT files|*.txt";
            open.InitialDirectory = @"C:\";

            if (open.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = open.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            string file = open.FileName;                 
                            client.BuildXML(file);
                            PopulateDonorsTable();
                            MessageBox.Show("Dadores importados com sucesso!");
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }

            
        }

        private void PopulateDonorsTable()
        {

            // Preenche a bindingList com a lista GetDonors
            var bindingList = new BindingList<Donor>(client.GetDonors());
            var source = new BindingSource(bindingList, null);
            donorsTable.DataSource = source;

            // Ordem das colunas
            donorsTable.Columns["Number"].DisplayIndex = 0;
            donorsTable.Columns["GivenName"].DisplayIndex = 1;
            donorsTable.Columns["Surname"].DisplayIndex = 2;
            donorsTable.Columns["Gender"].DisplayIndex = 3;
            donorsTable.Columns["Age"].DisplayIndex = 4;
            donorsTable.Columns["BloodType"].DisplayIndex = 5;

            // Colunas visíveis
            donorsTable.AutoGenerateColumns = false;
            for (int i = 0; i < donorsTable.Columns.Count; i++)
            {
                donorsTable.Columns[i].Visible = false;
            }

            donorsTable.Columns["Number"].Visible = true;
            donorsTable.Columns["Gender"].Visible = true;
            donorsTable.Columns["GivenName"].Visible = true;
            donorsTable.Columns["Surname"].Visible = true;
            donorsTable.Columns["Age"].Visible = true;
            donorsTable.Columns["BloodType"].Visible = true;
        }

        private void listDonors_Click(object sender, EventArgs e)
        {
            PopulateDonorsTable();
        }

        private void searchDonors()
        {
            string search = searchBox.Text;

            // Se a search box estiver vazio popula a tabela e mostra aviso
            if (search.Equals(""))
            {
                PopulateDonorsTable();
                MessageBox.Show("Campo de pesquisa vazio!");
            }

            // Se o radio button Name estiver checked preenche a bindingList com a lista GetDonorsByName
            if (radioButtonName.Checked && !search.Equals(""))
            {
                var bindingList = new BindingList<Donor>(client.GetDonorsByName(search));
                var source = new BindingSource(bindingList, null);
                donorsTable.DataSource = source;
            }

            // Se o radio button Age estiver checked preenche a bindingList com a lista GetDonorsByAge
            if (radioButtonAge.Checked && !search.Equals(""))
            {
                var bindingList = new BindingList<Donor>(client.GetDonorsByAge(search));
                var source = new BindingSource(bindingList, null);
                donorsTable.DataSource = source;
            }

            // Se o radio button BloodType estiver checked preenche a bindingList com a lista GetDonorsByBloodType
            if (radioButtonBloodType.Checked && !search.Equals(""))
            {
                var bindingList = new BindingList<Donor>(client.GetDonorsByBloodType(search));
                var source = new BindingSource(bindingList, null);
                donorsTable.DataSource = source;
            }

            // Colunas visíveis
            donorsTable.Columns["Number"].DisplayIndex = 0;
            donorsTable.Columns["GivenName"].DisplayIndex = 1;
            donorsTable.Columns["Surname"].DisplayIndex = 2;
            donorsTable.Columns["Gender"].DisplayIndex = 3;
            donorsTable.Columns["Age"].DisplayIndex = 4;
            donorsTable.Columns["BloodType"].DisplayIndex = 5;

            donorsTable.AutoGenerateColumns = false;
            for (int i = 0; i < donorsTable.Columns.Count; i++)
            {
                donorsTable.Columns[i].Visible = false;
            }

            donorsTable.Columns["Number"].Visible = true;
            donorsTable.Columns["Gender"].Visible = true;
            donorsTable.Columns["GivenName"].Visible = true;
            donorsTable.Columns["Surname"].Visible = true;
            donorsTable.Columns["Age"].Visible = true;
            donorsTable.Columns["BloodType"].Visible = true;
        }

        private void search_Click(object sender, EventArgs e)
        {
            searchDonors();

            // Se a tabela estiver vazia
            if (donorsTable.Rows.Count == 0)
            {
                PopulateDonorsTable();

                if(donorsTable.DataSource != null)
                {
                    MessageBox.Show("Não foram encontrados resultados!");
                }   
            }             
            
            // Se nenhum radio button estiver checked
            if (!radioButtonName.Checked && !radioButtonAge.Checked && !radioButtonBloodType.Checked)
            {
                MessageBox.Show("É obrigatório escolher um filtro de pesquisa!");
            }
        }

        private void showDetail_Click(object sender, EventArgs e)
        {

            // Se a tabela estiver vazia
            if (donorsTable.Rows.Count == 0)
            {
                MessageBox.Show("Nenhum dador selecionado!");
            }
            else
            {
                // Obter index
                int selectedIndex = donorsTable.SelectedCells[0].RowIndex;

                // Proteção do index
                if (selectedIndex < 0)
                {
                    MessageBox.Show("Nenhum dador selecionado!");
                }
                else
                {
                    // Obter id 
                    DataGridViewRow selectedRow = donorsTable.Rows[selectedIndex];
                    string id = Convert.ToString(selectedRow.Cells["Number"].Value);

                    // Enviar o number para o novo form
                    d.ShowDonorDetail(id);
                    d.ShowDialog();
                }
            }   
        }

        private void removeDonor_Click(object sender, EventArgs e)
        {
            // Se a tabela estiver vazia
            if (donorsTable.Rows.Count == 0)
            {
                MessageBox.Show("Nenhum dador selecionado!");
            }
            else
            {
                // Obter index
                int selectedIndex = donorsTable.SelectedCells[0].RowIndex;

                // Proteção do index
                if (selectedIndex < 0)
                {
                    MessageBox.Show("Nenhum dador selecionado!");
                }
                else
                {

                    DataGridViewRow selectedRow = donorsTable.Rows[selectedIndex];

                    // Obter nome completo
                    string givenName = Convert.ToString(selectedRow.Cells["GivenName"].Value);
                    string surname = Convert.ToString(selectedRow.Cells["Surname"].Value);

                    // Obter number
                    string number = Convert.ToString(selectedRow.Cells["Number"].Value);

                    // Confirmar
                    var confirmResult = MessageBox.Show("Tem a certeza que pretende eliminar o dador " + givenName + " " + surname + "?",
                                                        "Confirmação",
                                                        MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        // Enviar number para o método no webservice
                        client.DeleteDonorByNumber(number);

                        // Refresh na tabela
                        PopulateDonorsTable();
                    }
                    else
                    {

                    }
                }
            }
        }

        private void addDonor_Click(object sender, EventArgs e)
        {
            ad.ShowDialog();
        }

        private void Principal_Load(object sender, EventArgs e)
        {

        }

        private void donorsTable_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void listDonorsView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        
    }
}
