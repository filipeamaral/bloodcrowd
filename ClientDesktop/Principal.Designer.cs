﻿namespace ClientDesktop
{
    partial class Principal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.importDonors = new System.Windows.Forms.Button();
            this.donorsTable = new System.Windows.Forms.DataGridView();
            this.listDonors = new System.Windows.Forms.Button();
            this.addDonor = new System.Windows.Forms.Button();
            this.searchBox = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioButtonBloodType = new System.Windows.Forms.RadioButton();
            this.radioButtonAge = new System.Windows.Forms.RadioButton();
            this.radioButtonName = new System.Windows.Forms.RadioButton();
            this.search = new System.Windows.Forms.Button();
            this.removeDonor = new System.Windows.Forms.Button();
            this.showDetail = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.donorsTable)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // importDonors
            // 
            this.importDonors.BackColor = System.Drawing.SystemColors.ControlLight;
            this.importDonors.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.importDonors.Font = new System.Drawing.Font("Lucida Fax", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.importDonors.Location = new System.Drawing.Point(20, 22);
            this.importDonors.Name = "importDonors";
            this.importDonors.Size = new System.Drawing.Size(190, 39);
            this.importDonors.TabIndex = 0;
            this.importDonors.Text = "Importar Dados";
            this.importDonors.UseVisualStyleBackColor = false;
            this.importDonors.Click += new System.EventHandler(this.importDonors_Click);
            // 
            // donorsTable
            // 
            this.donorsTable.AllowUserToAddRows = false;
            this.donorsTable.AllowUserToDeleteRows = false;
            this.donorsTable.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.donorsTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.donorsTable.Location = new System.Drawing.Point(20, 67);
            this.donorsTable.Name = "donorsTable";
            this.donorsTable.RowHeadersVisible = false;
            this.donorsTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.donorsTable.Size = new System.Drawing.Size(622, 287);
            this.donorsTable.TabIndex = 1;
            this.donorsTable.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.donorsTable_CellContentClick);
            // 
            // listDonors
            // 
            this.listDonors.BackColor = System.Drawing.SystemColors.ControlLight;
            this.listDonors.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.listDonors.Font = new System.Drawing.Font("Lucida Fax", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listDonors.Location = new System.Drawing.Point(236, 22);
            this.listDonors.Name = "listDonors";
            this.listDonors.Size = new System.Drawing.Size(190, 39);
            this.listDonors.TabIndex = 2;
            this.listDonors.Text = "Listar Dadores";
            this.listDonors.UseVisualStyleBackColor = false;
            this.listDonors.Click += new System.EventHandler(this.listDonors_Click);
            // 
            // addDonor
            // 
            this.addDonor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.addDonor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.addDonor.Font = new System.Drawing.Font("Lucida Fax", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.addDonor.Location = new System.Drawing.Point(490, 360);
            this.addDonor.Name = "addDonor";
            this.addDonor.Size = new System.Drawing.Size(152, 34);
            this.addDonor.TabIndex = 3;
            this.addDonor.Text = "Adicionar Dador";
            this.addDonor.UseVisualStyleBackColor = false;
            this.addDonor.Click += new System.EventHandler(this.addDonor_Click);
            // 
            // searchBox
            // 
            this.searchBox.Location = new System.Drawing.Point(20, 360);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(193, 26);
            this.searchBox.TabIndex = 4;
            this.searchBox.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radioButtonBloodType);
            this.panel1.Controls.Add(this.radioButtonAge);
            this.panel1.Controls.Add(this.radioButtonName);
            this.panel1.Location = new System.Drawing.Point(20, 392);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(142, 76);
            this.panel1.TabIndex = 5;
            // 
            // radioButtonBloodType
            // 
            this.radioButtonBloodType.AutoSize = true;
            this.radioButtonBloodType.Font = new System.Drawing.Font("Lucida Fax", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonBloodType.Location = new System.Drawing.Point(4, 54);
            this.radioButtonBloodType.Name = "radioButtonBloodType";
            this.radioButtonBloodType.Size = new System.Drawing.Size(131, 19);
            this.radioButtonBloodType.TabIndex = 2;
            this.radioButtonBloodType.TabStop = true;
            this.radioButtonBloodType.Text = "Grupo Sanguíneo";
            this.radioButtonBloodType.UseVisualStyleBackColor = true;
            // 
            // radioButtonAge
            // 
            this.radioButtonAge.AutoSize = true;
            this.radioButtonAge.Font = new System.Drawing.Font("Lucida Fax", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonAge.Location = new System.Drawing.Point(4, 28);
            this.radioButtonAge.Name = "radioButtonAge";
            this.radioButtonAge.Size = new System.Drawing.Size(59, 19);
            this.radioButtonAge.TabIndex = 1;
            this.radioButtonAge.TabStop = true;
            this.radioButtonAge.Text = "Idade";
            this.radioButtonAge.UseVisualStyleBackColor = true;
            // 
            // radioButtonName
            // 
            this.radioButtonName.AutoSize = true;
            this.radioButtonName.Font = new System.Drawing.Font("Lucida Fax", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButtonName.Location = new System.Drawing.Point(4, 4);
            this.radioButtonName.Name = "radioButtonName";
            this.radioButtonName.Size = new System.Drawing.Size(61, 19);
            this.radioButtonName.TabIndex = 0;
            this.radioButtonName.TabStop = true;
            this.radioButtonName.Text = "Nome";
            this.radioButtonName.UseVisualStyleBackColor = true;
            // 
            // search
            // 
            this.search.BackColor = System.Drawing.SystemColors.ControlLight;
            this.search.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.search.Font = new System.Drawing.Font("Lucida Fax", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.search.Location = new System.Drawing.Point(219, 360);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(98, 26);
            this.search.TabIndex = 6;
            this.search.Text = "Pesquisar";
            this.search.UseVisualStyleBackColor = false;
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // removeDonor
            // 
            this.removeDonor.BackColor = System.Drawing.SystemColors.ControlLight;
            this.removeDonor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.removeDonor.Font = new System.Drawing.Font("Lucida Fax", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.removeDonor.Location = new System.Drawing.Point(490, 400);
            this.removeDonor.Name = "removeDonor";
            this.removeDonor.Size = new System.Drawing.Size(152, 34);
            this.removeDonor.TabIndex = 7;
            this.removeDonor.Text = "Remover Dador";
            this.removeDonor.UseVisualStyleBackColor = false;
            this.removeDonor.Click += new System.EventHandler(this.removeDonor_Click);
            // 
            // showDetail
            // 
            this.showDetail.BackColor = System.Drawing.SystemColors.ControlLight;
            this.showDetail.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.showDetail.Font = new System.Drawing.Font("Lucida Fax", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.showDetail.Location = new System.Drawing.Point(452, 22);
            this.showDetail.Name = "showDetail";
            this.showDetail.Size = new System.Drawing.Size(190, 39);
            this.showDetail.TabIndex = 8;
            this.showDetail.Text = "Ver Detalhes";
            this.showDetail.UseVisualStyleBackColor = false;
            this.showDetail.Click += new System.EventHandler(this.showDetail_Click);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 494);
            this.Controls.Add(this.showDetail);
            this.Controls.Add(this.removeDonor);
            this.Controls.Add(this.search);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.addDonor);
            this.Controls.Add(this.listDonors);
            this.Controls.Add(this.donorsTable);
            this.Controls.Add(this.importDonors);
            this.Font = new System.Drawing.Font("Lucida Fax", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "Principal";
            this.Text = "BloodCrowd";
            this.Load += new System.EventHandler(this.Principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.donorsTable)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button importDonors;
        private System.Windows.Forms.Button listDonors;
        private System.Windows.Forms.Button addDonor;
        private System.Windows.Forms.RichTextBox searchBox;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonBloodType;
        private System.Windows.Forms.RadioButton radioButtonAge;
        private System.Windows.Forms.RadioButton radioButtonName;
        private System.Windows.Forms.Button search;
        private System.Windows.Forms.Button removeDonor;
        private System.Windows.Forms.DataGridView donorsTable;
        private System.Windows.Forms.Button showDetail;
    }
}

