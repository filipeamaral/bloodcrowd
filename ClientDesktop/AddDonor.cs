﻿using ClientDesktop.ServiceBloodcrowd;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientDesktop
{
    public partial class AddDonor : Form
    {
        ServiceBloodcrowdClient client = new ServiceBloodcrowdClient();

        int age;
        string gender;
        string bloodType;
        int lastNumber;
        int number;
        string password;
        double testDouble;
        int testInt;
        long testLong;

        public AddDonor()
        {
            InitializeComponent();
            genderComboBox.Items.Add("male");
            genderComboBox.Items.Add("female");
            bloodTypeComboBox.Items.Add("O-");
            bloodTypeComboBox.Items.Add("O+");
            bloodTypeComboBox.Items.Add("A-");
            bloodTypeComboBox.Items.Add("A+");
            bloodTypeComboBox.Items.Add("B-");
            bloodTypeComboBox.Items.Add("B+");
            bloodTypeComboBox.Items.Add("AB-");
            bloodTypeComboBox.Items.Add("AB+");
        }

        private void buttonAddDonor_Click(object sender, EventArgs e)
        {
            // Gera password aleatória
            password = client.GeneratePassword();

            // Obtem o número do último donor e adiciona mais 1
            lastNumber = Convert.ToInt32(client.GetLastNumber());
            number = lastNumber + 1;

            // Validação de espaços ou null
            if (string.IsNullOrWhiteSpace(givenNameTextBox.Text) || string.IsNullOrWhiteSpace(surnameTextBox.Text) ||
                string.IsNullOrWhiteSpace(streetAddressTextBox.Text) || string.IsNullOrWhiteSpace(cityTextBox.Text) ||
                string.IsNullOrWhiteSpace(statefullTextBox.Text) || string.IsNullOrWhiteSpace(zipCodeTextBox.Text) ||
                string.IsNullOrWhiteSpace(emailTextBox.Text) || string.IsNullOrWhiteSpace(usernameTextBox.Text) ||
                string.IsNullOrWhiteSpace(password) || string.IsNullOrWhiteSpace(telephoneTextBox.Text) ||
                string.IsNullOrWhiteSpace(mothersMaidenTextBox.Text) || string.IsNullOrWhiteSpace(occupationTextBox.Text) ||
                string.IsNullOrWhiteSpace(companyTextBox.Text) || string.IsNullOrWhiteSpace(vehicleTextBox.Text) ||
                string.IsNullOrWhiteSpace(guidTextBox.Text) || string.IsNullOrWhiteSpace(birthdayDateTimePicker.ToString()) ||
                string.IsNullOrWhiteSpace(kilogramsTextBox.Text) || string.IsNullOrWhiteSpace(centimetersTextBox.Text) ||
                string.IsNullOrWhiteSpace(latitudeTextBox.Text) || string.IsNullOrWhiteSpace(longitudeTextBox.Text) ||
                gender == null || bloodType == null)
            {
                MessageBox.Show("Os campos são todos obrigatórios!");

            }

            // Valida o formato double
            if (!Double.TryParse(kilogramsTextBox.Text, out testDouble))
            {
                MessageBox.Show("Erro no campo Peso! (ex.: 50,1)");
            }

            // Valida o formato double
            if (!Double.TryParse(longitudeTextBox.Text, out testDouble))
            {
                MessageBox.Show("Erro no campo Longitude! (ex.: -5,123321)");
            }

            // Valida o formato double
            if (!Double.TryParse(latitudeTextBox.Text, out testDouble))
            {
                MessageBox.Show("Erro no campo Longitude! (ex.: 5,123321)");
            }

            // Valida o formato int
            if (!Int32.TryParse(centimetersTextBox.Text, out testInt) || centimetersTextBox.Text.Length > 3)
            {
                MessageBox.Show("Erro no campo Altura! (ex.: 180)");
            }

            // Valida o formato long
            if (!Int64.TryParse(telephoneTextBox.Text, out testLong) || telephoneTextBox.Text.Length != 9)
            {
                MessageBox.Show("Erro no campo Telefone! (ex.: 912345678)");
            }
            else
            {
                Donor donor = new Donor
                {
                    Number = number,
                    GivenName = givenNameTextBox.Text,
                    Surname = surnameTextBox.Text,
                    Gender = gender,
                    StreetAddress = streetAddressTextBox.Text,
                    City = cityTextBox.Text,
                    StateFull = statefullTextBox.Text,
                    ZipCode = zipCodeTextBox.Text,
                    Email = emailTextBox.Text,
                    Username = usernameTextBox.Text,
                    Password = password,
                    PhoneNumber = Convert.ToInt64(telephoneTextBox.Text),
                    MothersMaiden = mothersMaidenTextBox.Text,
                    Birthday = birthdayDateTimePicker.Value.ToShortDateString(),
                    Age = CalculateAge(),
                    Occupation = occupationTextBox.Text,
                    Company = companyTextBox.Text,
                    Vehicle = vehicleTextBox.Text,
                    BloodType = bloodType,
                    Kilograms = Convert.ToDouble(kilogramsTextBox.Text, NumberFormatInfo.InvariantInfo),
                    Centimeters = Convert.ToInt32(centimetersTextBox.Text),
                    Guid = guidTextBox.Text,
                    Latitude = Convert.ToDouble(latitudeTextBox.Text, NumberFormatInfo.InvariantInfo),
                    Longitude = Convert.ToDouble(longitudeTextBox.Text, NumberFormatInfo.InvariantInfo)
                };

                client.AddDonor(donor);
                MessageBox.Show("Dador adicionado com sucesso!");
                Close();
            }      
        }
        
        // Valor selecionado do sexo
        private void genderComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            gender = genderComboBox.SelectedItem.ToString();
        }

        // Valor selecionado do bloodType
        private void bloodTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            bloodType = bloodTypeComboBox.SelectedItem.ToString();
        }

        // Calcula a idade
        private int CalculateAge()
        {
            age = (DateTime.Now.Year - birthdayDateTimePicker.Value.Year -
                  (DateTime.Now.DayOfYear < birthdayDateTimePicker.Value.DayOfYear ? 1 : 0));

            return age;
        }

        private void AddDonor_Load(object sender, EventArgs e)
        {

        }
    }
}
