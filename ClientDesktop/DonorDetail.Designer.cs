﻿namespace ClientDesktop
{
    partial class DonorDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelName = new System.Windows.Forms.Label();
            this.fullNameTextBox = new System.Windows.Forms.RichTextBox();
            this.labelFullName = new System.Windows.Forms.Label();
            this.labelMorada = new System.Windows.Forms.Label();
            this.streetAddressTextBox = new System.Windows.Forms.RichTextBox();
            this.labelCidade = new System.Windows.Forms.Label();
            this.cityTextBox = new System.Windows.Forms.RichTextBox();
            this.labelUsername = new System.Windows.Forms.Label();
            this.usernameTextBox = new System.Windows.Forms.RichTextBox();
            this.labelTelephone = new System.Windows.Forms.Label();
            this.telephoneTextBox = new System.Windows.Forms.RichTextBox();
            this.labelMothersMaiden = new System.Windows.Forms.Label();
            this.mothersMaidenTextBox = new System.Windows.Forms.RichTextBox();
            this.statefullTextBox = new System.Windows.Forms.RichTextBox();
            this.labelStatefull = new System.Windows.Forms.Label();
            this.zipCodeTextBox = new System.Windows.Forms.RichTextBox();
            this.labelZipCode = new System.Windows.Forms.Label();
            this.emailTextBox = new System.Windows.Forms.RichTextBox();
            this.labelEmail = new System.Windows.Forms.Label();
            this.birthdayTextBox = new System.Windows.Forms.RichTextBox();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.ageTextBox = new System.Windows.Forms.RichTextBox();
            this.labelAge = new System.Windows.Forms.Label();
            this.occupationTextBox = new System.Windows.Forms.RichTextBox();
            this.labelOccupation = new System.Windows.Forms.Label();
            this.labelBloodType = new System.Windows.Forms.Label();
            this.bloodtypeTextBox = new System.Windows.Forms.RichTextBox();
            this.labelVehicle = new System.Windows.Forms.Label();
            this.vehicleTextBox = new System.Windows.Forms.RichTextBox();
            this.labelCompany = new System.Windows.Forms.Label();
            this.companyTextBox = new System.Windows.Forms.RichTextBox();
            this.labelGUID = new System.Windows.Forms.Label();
            this.guidTextBox = new System.Windows.Forms.RichTextBox();
            this.labelCentimeters = new System.Windows.Forms.Label();
            this.centimetersTextBox = new System.Windows.Forms.RichTextBox();
            this.labelKilograms = new System.Windows.Forms.Label();
            this.kilogramsTextBox = new System.Windows.Forms.RichTextBox();
            this.labelLongitude = new System.Windows.Forms.Label();
            this.longitudeTextBox = new System.Windows.Forms.RichTextBox();
            this.labelLatitude = new System.Windows.Forms.Label();
            this.latitudeTextBox = new System.Windows.Forms.RichTextBox();
            this.genderLabel = new System.Windows.Forms.Label();
            this.genderTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(29, 34);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(0, 13);
            this.labelName.TabIndex = 0;
            // 
            // fullNameTextBox
            // 
            this.fullNameTextBox.BackColor = System.Drawing.Color.White;
            this.fullNameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fullNameTextBox.Location = new System.Drawing.Point(120, 31);
            this.fullNameTextBox.Name = "fullNameTextBox";
            this.fullNameTextBox.ReadOnly = true;
            this.fullNameTextBox.Size = new System.Drawing.Size(254, 26);
            this.fullNameTextBox.TabIndex = 1;
            this.fullNameTextBox.Text = "";
            // 
            // labelFullName
            // 
            this.labelFullName.AutoSize = true;
            this.labelFullName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFullName.Location = new System.Drawing.Point(69, 34);
            this.labelFullName.Name = "labelFullName";
            this.labelFullName.Size = new System.Drawing.Size(45, 16);
            this.labelFullName.TabIndex = 2;
            this.labelFullName.Text = "Nome";
            // 
            // labelMorada
            // 
            this.labelMorada.AutoSize = true;
            this.labelMorada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMorada.Location = new System.Drawing.Point(59, 97);
            this.labelMorada.Name = "labelMorada";
            this.labelMorada.Size = new System.Drawing.Size(55, 16);
            this.labelMorada.TabIndex = 4;
            this.labelMorada.Text = "Morada";
            // 
            // streetAddressTextBox
            // 
            this.streetAddressTextBox.BackColor = System.Drawing.Color.White;
            this.streetAddressTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.streetAddressTextBox.Location = new System.Drawing.Point(120, 96);
            this.streetAddressTextBox.Name = "streetAddressTextBox";
            this.streetAddressTextBox.ReadOnly = true;
            this.streetAddressTextBox.Size = new System.Drawing.Size(254, 27);
            this.streetAddressTextBox.TabIndex = 3;
            this.streetAddressTextBox.Text = "";
            // 
            // labelCidade
            // 
            this.labelCidade.AutoSize = true;
            this.labelCidade.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCidade.Location = new System.Drawing.Point(62, 130);
            this.labelCidade.Name = "labelCidade";
            this.labelCidade.Size = new System.Drawing.Size(52, 16);
            this.labelCidade.TabIndex = 6;
            this.labelCidade.Text = "Cidade";
            // 
            // cityTextBox
            // 
            this.cityTextBox.BackColor = System.Drawing.Color.White;
            this.cityTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cityTextBox.Location = new System.Drawing.Point(120, 129);
            this.cityTextBox.Name = "cityTextBox";
            this.cityTextBox.ReadOnly = true;
            this.cityTextBox.Size = new System.Drawing.Size(254, 27);
            this.cityTextBox.TabIndex = 5;
            this.cityTextBox.Text = "";
            this.cityTextBox.TextChanged += new System.EventHandler(this.cityTextBox_TextChanged);
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUsername.Location = new System.Drawing.Point(474, 34);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(64, 16);
            this.labelUsername.TabIndex = 14;
            this.labelUsername.Text = "Utilizador";
            this.labelUsername.Click += new System.EventHandler(this.labelUsername_Click);
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.BackColor = System.Drawing.Color.White;
            this.usernameTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.usernameTextBox.Location = new System.Drawing.Point(544, 31);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.ReadOnly = true;
            this.usernameTextBox.Size = new System.Drawing.Size(254, 26);
            this.usernameTextBox.TabIndex = 13;
            this.usernameTextBox.Text = "";
            // 
            // labelTelephone
            // 
            this.labelTelephone.AutoSize = true;
            this.labelTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTelephone.Location = new System.Drawing.Point(476, 67);
            this.labelTelephone.Name = "labelTelephone";
            this.labelTelephone.Size = new System.Drawing.Size(62, 16);
            this.labelTelephone.TabIndex = 16;
            this.labelTelephone.Text = "Telefone";
            // 
            // telephoneTextBox
            // 
            this.telephoneTextBox.BackColor = System.Drawing.Color.White;
            this.telephoneTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telephoneTextBox.Location = new System.Drawing.Point(544, 64);
            this.telephoneTextBox.Name = "telephoneTextBox";
            this.telephoneTextBox.ReadOnly = true;
            this.telephoneTextBox.Size = new System.Drawing.Size(254, 26);
            this.telephoneTextBox.TabIndex = 15;
            this.telephoneTextBox.Text = "";
            // 
            // labelMothersMaiden
            // 
            this.labelMothersMaiden.AutoSize = true;
            this.labelMothersMaiden.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMothersMaiden.Location = new System.Drawing.Point(444, 99);
            this.labelMothersMaiden.Name = "labelMothersMaiden";
            this.labelMothersMaiden.Size = new System.Drawing.Size(94, 16);
            this.labelMothersMaiden.TabIndex = 18;
            this.labelMothersMaiden.Text = "Nome da Mãe";
            // 
            // mothersMaidenTextBox
            // 
            this.mothersMaidenTextBox.BackColor = System.Drawing.Color.White;
            this.mothersMaidenTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mothersMaidenTextBox.Location = new System.Drawing.Point(544, 96);
            this.mothersMaidenTextBox.Name = "mothersMaidenTextBox";
            this.mothersMaidenTextBox.ReadOnly = true;
            this.mothersMaidenTextBox.Size = new System.Drawing.Size(254, 27);
            this.mothersMaidenTextBox.TabIndex = 17;
            this.mothersMaidenTextBox.Text = "";
            // 
            // statefullTextBox
            // 
            this.statefullTextBox.BackColor = System.Drawing.Color.White;
            this.statefullTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.statefullTextBox.Location = new System.Drawing.Point(120, 162);
            this.statefullTextBox.Name = "statefullTextBox";
            this.statefullTextBox.ReadOnly = true;
            this.statefullTextBox.Size = new System.Drawing.Size(254, 27);
            this.statefullTextBox.TabIndex = 7;
            this.statefullTextBox.Text = "";
            // 
            // labelStatefull
            // 
            this.labelStatefull.AutoSize = true;
            this.labelStatefull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStatefull.Location = new System.Drawing.Point(65, 163);
            this.labelStatefull.Name = "labelStatefull";
            this.labelStatefull.Size = new System.Drawing.Size(49, 16);
            this.labelStatefull.TabIndex = 8;
            this.labelStatefull.Text = "Distrito";
            // 
            // zipCodeTextBox
            // 
            this.zipCodeTextBox.BackColor = System.Drawing.Color.White;
            this.zipCodeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zipCodeTextBox.Location = new System.Drawing.Point(120, 195);
            this.zipCodeTextBox.Name = "zipCodeTextBox";
            this.zipCodeTextBox.ReadOnly = true;
            this.zipCodeTextBox.Size = new System.Drawing.Size(254, 26);
            this.zipCodeTextBox.TabIndex = 9;
            this.zipCodeTextBox.Text = "";
            // 
            // labelZipCode
            // 
            this.labelZipCode.AutoSize = true;
            this.labelZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZipCode.Location = new System.Drawing.Point(21, 196);
            this.labelZipCode.Name = "labelZipCode";
            this.labelZipCode.Size = new System.Drawing.Size(93, 16);
            this.labelZipCode.TabIndex = 10;
            this.labelZipCode.Text = "Código Postal";
            // 
            // emailTextBox
            // 
            this.emailTextBox.BackColor = System.Drawing.Color.White;
            this.emailTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.emailTextBox.Location = new System.Drawing.Point(120, 227);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.ReadOnly = true;
            this.emailTextBox.Size = new System.Drawing.Size(254, 27);
            this.emailTextBox.TabIndex = 11;
            this.emailTextBox.Text = "";
            // 
            // labelEmail
            // 
            this.labelEmail.AutoSize = true;
            this.labelEmail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEmail.Location = new System.Drawing.Point(72, 228);
            this.labelEmail.Name = "labelEmail";
            this.labelEmail.Size = new System.Drawing.Size(42, 16);
            this.labelEmail.TabIndex = 12;
            this.labelEmail.Text = "Email";
            // 
            // birthdayTextBox
            // 
            this.birthdayTextBox.BackColor = System.Drawing.Color.White;
            this.birthdayTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthdayTextBox.Location = new System.Drawing.Point(544, 129);
            this.birthdayTextBox.Name = "birthdayTextBox";
            this.birthdayTextBox.ReadOnly = true;
            this.birthdayTextBox.Size = new System.Drawing.Size(254, 27);
            this.birthdayTextBox.TabIndex = 19;
            this.birthdayTextBox.Text = "";
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBirthday.Location = new System.Drawing.Point(407, 132);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(131, 16);
            this.labelBirthday.TabIndex = 20;
            this.labelBirthday.Text = "Data de Nascimento";
            // 
            // ageTextBox
            // 
            this.ageTextBox.BackColor = System.Drawing.Color.White;
            this.ageTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ageTextBox.Location = new System.Drawing.Point(544, 163);
            this.ageTextBox.Name = "ageTextBox";
            this.ageTextBox.ReadOnly = true;
            this.ageTextBox.Size = new System.Drawing.Size(254, 26);
            this.ageTextBox.TabIndex = 21;
            this.ageTextBox.Text = "";
            // 
            // labelAge
            // 
            this.labelAge.AutoSize = true;
            this.labelAge.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAge.Location = new System.Drawing.Point(495, 166);
            this.labelAge.Name = "labelAge";
            this.labelAge.Size = new System.Drawing.Size(43, 16);
            this.labelAge.TabIndex = 22;
            this.labelAge.Text = "Idade";
            // 
            // occupationTextBox
            // 
            this.occupationTextBox.BackColor = System.Drawing.Color.White;
            this.occupationTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.occupationTextBox.Location = new System.Drawing.Point(544, 195);
            this.occupationTextBox.Name = "occupationTextBox";
            this.occupationTextBox.ReadOnly = true;
            this.occupationTextBox.Size = new System.Drawing.Size(254, 26);
            this.occupationTextBox.TabIndex = 23;
            this.occupationTextBox.Text = "";
            // 
            // labelOccupation
            // 
            this.labelOccupation.AutoSize = true;
            this.labelOccupation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOccupation.Location = new System.Drawing.Point(467, 198);
            this.labelOccupation.Name = "labelOccupation";
            this.labelOccupation.Size = new System.Drawing.Size(71, 16);
            this.labelOccupation.TabIndex = 24;
            this.labelOccupation.Text = "Ocupação";
            // 
            // labelBloodType
            // 
            this.labelBloodType.AutoSize = true;
            this.labelBloodType.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBloodType.Location = new System.Drawing.Point(425, 296);
            this.labelBloodType.Name = "labelBloodType";
            this.labelBloodType.Size = new System.Drawing.Size(113, 16);
            this.labelBloodType.TabIndex = 37;
            this.labelBloodType.Text = "Grupo Sanguíneo";
            // 
            // bloodtypeTextBox
            // 
            this.bloodtypeTextBox.BackColor = System.Drawing.Color.White;
            this.bloodtypeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bloodtypeTextBox.Location = new System.Drawing.Point(544, 293);
            this.bloodtypeTextBox.Name = "bloodtypeTextBox";
            this.bloodtypeTextBox.ReadOnly = true;
            this.bloodtypeTextBox.Size = new System.Drawing.Size(254, 27);
            this.bloodtypeTextBox.TabIndex = 36;
            this.bloodtypeTextBox.Text = "";
            // 
            // labelVehicle
            // 
            this.labelVehicle.AutoSize = true;
            this.labelVehicle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVehicle.Location = new System.Drawing.Point(485, 264);
            this.labelVehicle.Name = "labelVehicle";
            this.labelVehicle.Size = new System.Drawing.Size(53, 16);
            this.labelVehicle.TabIndex = 35;
            this.labelVehicle.Text = "Veículo";
            // 
            // vehicleTextBox
            // 
            this.vehicleTextBox.BackColor = System.Drawing.Color.White;
            this.vehicleTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vehicleTextBox.Location = new System.Drawing.Point(544, 261);
            this.vehicleTextBox.Name = "vehicleTextBox";
            this.vehicleTextBox.ReadOnly = true;
            this.vehicleTextBox.Size = new System.Drawing.Size(254, 26);
            this.vehicleTextBox.TabIndex = 34;
            this.vehicleTextBox.Text = "";
            // 
            // labelCompany
            // 
            this.labelCompany.AutoSize = true;
            this.labelCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompany.Location = new System.Drawing.Point(475, 231);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(63, 16);
            this.labelCompany.TabIndex = 33;
            this.labelCompany.Text = "Empresa";
            // 
            // companyTextBox
            // 
            this.companyTextBox.BackColor = System.Drawing.Color.White;
            this.companyTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.companyTextBox.Location = new System.Drawing.Point(544, 228);
            this.companyTextBox.Name = "companyTextBox";
            this.companyTextBox.ReadOnly = true;
            this.companyTextBox.Size = new System.Drawing.Size(254, 26);
            this.companyTextBox.TabIndex = 32;
            this.companyTextBox.Text = "";
            // 
            // labelGUID
            // 
            this.labelGUID.AutoSize = true;
            this.labelGUID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGUID.Location = new System.Drawing.Point(73, 327);
            this.labelGUID.Name = "labelGUID";
            this.labelGUID.Size = new System.Drawing.Size(41, 16);
            this.labelGUID.TabIndex = 31;
            this.labelGUID.Text = "GUID";
            // 
            // guidTextBox
            // 
            this.guidTextBox.BackColor = System.Drawing.Color.White;
            this.guidTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guidTextBox.Location = new System.Drawing.Point(120, 326);
            this.guidTextBox.Name = "guidTextBox";
            this.guidTextBox.ReadOnly = true;
            this.guidTextBox.Size = new System.Drawing.Size(254, 27);
            this.guidTextBox.TabIndex = 30;
            this.guidTextBox.Text = "";
            this.guidTextBox.TextChanged += new System.EventHandler(this.guidTextBox_TextChanged);
            // 
            // labelCentimeters
            // 
            this.labelCentimeters.AutoSize = true;
            this.labelCentimeters.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCentimeters.Location = new System.Drawing.Point(43, 294);
            this.labelCentimeters.Name = "labelCentimeters";
            this.labelCentimeters.Size = new System.Drawing.Size(71, 16);
            this.labelCentimeters.TabIndex = 29;
            this.labelCentimeters.Text = "Altura (cm)";
            // 
            // centimetersTextBox
            // 
            this.centimetersTextBox.BackColor = System.Drawing.Color.White;
            this.centimetersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.centimetersTextBox.Location = new System.Drawing.Point(120, 293);
            this.centimetersTextBox.Name = "centimetersTextBox";
            this.centimetersTextBox.ReadOnly = true;
            this.centimetersTextBox.Size = new System.Drawing.Size(254, 27);
            this.centimetersTextBox.TabIndex = 28;
            this.centimetersTextBox.Text = "";
            // 
            // labelKilograms
            // 
            this.labelKilograms.AutoSize = true;
            this.labelKilograms.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKilograms.Location = new System.Drawing.Point(48, 261);
            this.labelKilograms.Name = "labelKilograms";
            this.labelKilograms.Size = new System.Drawing.Size(66, 16);
            this.labelKilograms.TabIndex = 27;
            this.labelKilograms.Text = "Peso (kg)";
            // 
            // kilogramsTextBox
            // 
            this.kilogramsTextBox.BackColor = System.Drawing.Color.White;
            this.kilogramsTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kilogramsTextBox.Location = new System.Drawing.Point(120, 260);
            this.kilogramsTextBox.Name = "kilogramsTextBox";
            this.kilogramsTextBox.ReadOnly = true;
            this.kilogramsTextBox.Size = new System.Drawing.Size(254, 27);
            this.kilogramsTextBox.TabIndex = 26;
            this.kilogramsTextBox.Text = "";
            // 
            // labelLongitude
            // 
            this.labelLongitude.AutoSize = true;
            this.labelLongitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLongitude.Location = new System.Drawing.Point(471, 330);
            this.labelLongitude.Name = "labelLongitude";
            this.labelLongitude.Size = new System.Drawing.Size(67, 16);
            this.labelLongitude.TabIndex = 41;
            this.labelLongitude.Text = "Longitude";
            // 
            // longitudeTextBox
            // 
            this.longitudeTextBox.BackColor = System.Drawing.Color.White;
            this.longitudeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.longitudeTextBox.Location = new System.Drawing.Point(544, 327);
            this.longitudeTextBox.Name = "longitudeTextBox";
            this.longitudeTextBox.ReadOnly = true;
            this.longitudeTextBox.Size = new System.Drawing.Size(254, 26);
            this.longitudeTextBox.TabIndex = 40;
            this.longitudeTextBox.Text = "";
            this.longitudeTextBox.TextChanged += new System.EventHandler(this.richTextBox20_TextChanged);
            // 
            // labelLatitude
            // 
            this.labelLatitude.AutoSize = true;
            this.labelLatitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLatitude.Location = new System.Drawing.Point(59, 360);
            this.labelLatitude.Name = "labelLatitude";
            this.labelLatitude.Size = new System.Drawing.Size(55, 16);
            this.labelLatitude.TabIndex = 39;
            this.labelLatitude.Text = "Latitude";
            // 
            // latitudeTextBox
            // 
            this.latitudeTextBox.BackColor = System.Drawing.Color.White;
            this.latitudeTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.latitudeTextBox.Location = new System.Drawing.Point(120, 359);
            this.latitudeTextBox.Name = "latitudeTextBox";
            this.latitudeTextBox.ReadOnly = true;
            this.latitudeTextBox.Size = new System.Drawing.Size(254, 27);
            this.latitudeTextBox.TabIndex = 38;
            this.latitudeTextBox.Text = "";
            // 
            // genderLabel
            // 
            this.genderLabel.AutoSize = true;
            this.genderLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genderLabel.Location = new System.Drawing.Point(75, 64);
            this.genderLabel.Name = "genderLabel";
            this.genderLabel.Size = new System.Drawing.Size(39, 16);
            this.genderLabel.TabIndex = 43;
            this.genderLabel.Text = "Sexo";
            // 
            // genderTextBox
            // 
            this.genderTextBox.BackColor = System.Drawing.Color.White;
            this.genderTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.genderTextBox.Location = new System.Drawing.Point(120, 63);
            this.genderTextBox.Name = "genderTextBox";
            this.genderTextBox.ReadOnly = true;
            this.genderTextBox.Size = new System.Drawing.Size(254, 27);
            this.genderTextBox.TabIndex = 42;
            this.genderTextBox.Text = "";
            // 
            // DonorDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(838, 408);
            this.Controls.Add(this.genderLabel);
            this.Controls.Add(this.genderTextBox);
            this.Controls.Add(this.labelLongitude);
            this.Controls.Add(this.longitudeTextBox);
            this.Controls.Add(this.labelLatitude);
            this.Controls.Add(this.latitudeTextBox);
            this.Controls.Add(this.labelBloodType);
            this.Controls.Add(this.bloodtypeTextBox);
            this.Controls.Add(this.labelVehicle);
            this.Controls.Add(this.vehicleTextBox);
            this.Controls.Add(this.labelCompany);
            this.Controls.Add(this.companyTextBox);
            this.Controls.Add(this.labelGUID);
            this.Controls.Add(this.guidTextBox);
            this.Controls.Add(this.labelCentimeters);
            this.Controls.Add(this.centimetersTextBox);
            this.Controls.Add(this.labelKilograms);
            this.Controls.Add(this.kilogramsTextBox);
            this.Controls.Add(this.labelOccupation);
            this.Controls.Add(this.occupationTextBox);
            this.Controls.Add(this.labelAge);
            this.Controls.Add(this.ageTextBox);
            this.Controls.Add(this.labelBirthday);
            this.Controls.Add(this.birthdayTextBox);
            this.Controls.Add(this.labelMothersMaiden);
            this.Controls.Add(this.mothersMaidenTextBox);
            this.Controls.Add(this.labelTelephone);
            this.Controls.Add(this.telephoneTextBox);
            this.Controls.Add(this.labelUsername);
            this.Controls.Add(this.usernameTextBox);
            this.Controls.Add(this.labelEmail);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(this.labelZipCode);
            this.Controls.Add(this.zipCodeTextBox);
            this.Controls.Add(this.labelStatefull);
            this.Controls.Add(this.statefullTextBox);
            this.Controls.Add(this.labelCidade);
            this.Controls.Add(this.cityTextBox);
            this.Controls.Add(this.labelMorada);
            this.Controls.Add(this.streetAddressTextBox);
            this.Controls.Add(this.labelFullName);
            this.Controls.Add(this.fullNameTextBox);
            this.Controls.Add(this.labelName);
            this.Name = "DonorDetail";
            this.Text = "Detalhes do Dador";
            this.Load += new System.EventHandler(this.DonorDetail_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.RichTextBox fullNameTextBox;
        private System.Windows.Forms.Label labelFullName;
        private System.Windows.Forms.Label labelMorada;
        private System.Windows.Forms.RichTextBox streetAddressTextBox;
        private System.Windows.Forms.Label labelCidade;
        private System.Windows.Forms.RichTextBox cityTextBox;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.RichTextBox usernameTextBox;
        private System.Windows.Forms.Label labelTelephone;
        private System.Windows.Forms.RichTextBox telephoneTextBox;
        private System.Windows.Forms.Label labelMothersMaiden;
        private System.Windows.Forms.RichTextBox mothersMaidenTextBox;
        private System.Windows.Forms.RichTextBox statefullTextBox;
        private System.Windows.Forms.Label labelStatefull;
        private System.Windows.Forms.RichTextBox zipCodeTextBox;
        private System.Windows.Forms.Label labelZipCode;
        private System.Windows.Forms.RichTextBox emailTextBox;
        private System.Windows.Forms.Label labelEmail;
        private System.Windows.Forms.RichTextBox birthdayTextBox;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.RichTextBox ageTextBox;
        private System.Windows.Forms.Label labelAge;
        private System.Windows.Forms.RichTextBox occupationTextBox;
        private System.Windows.Forms.Label labelOccupation;
        private System.Windows.Forms.Label labelBloodType;
        private System.Windows.Forms.RichTextBox bloodtypeTextBox;
        private System.Windows.Forms.Label labelVehicle;
        private System.Windows.Forms.RichTextBox vehicleTextBox;
        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.RichTextBox companyTextBox;
        private System.Windows.Forms.Label labelGUID;
        private System.Windows.Forms.RichTextBox guidTextBox;
        private System.Windows.Forms.Label labelCentimeters;
        private System.Windows.Forms.RichTextBox centimetersTextBox;
        private System.Windows.Forms.Label labelKilograms;
        private System.Windows.Forms.RichTextBox kilogramsTextBox;
        private System.Windows.Forms.Label labelLongitude;
        private System.Windows.Forms.RichTextBox longitudeTextBox;
        private System.Windows.Forms.Label labelLatitude;
        private System.Windows.Forms.RichTextBox latitudeTextBox;
        private System.Windows.Forms.Label genderLabel;
        private System.Windows.Forms.RichTextBox genderTextBox;
    }
}